var Todos = require("../models/todoModel");

module.exports = function(app) {
  app.get("/api/setupTodos", function(req, res) {
    // SEED DATA

    var startedTodos = [
      {
        username: "Lalit",
        todo: "Node JS",
        isDone: false,
        hasAttachment: false,
      },
      {
        username: "Sonu",
        todo: "React JS",
        isDone: false,
        hasAttachment: false,
      },
      {
        username: "Shahi",
        todo: "MongoDB",
        isDone: false,
        hasAttachment: false,
      },
    ];

    Todos.create(startedTodos, function(err, results) {
      res.send(results);
    });
  });
};
